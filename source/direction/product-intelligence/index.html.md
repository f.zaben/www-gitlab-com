---
layout: markdown_page
noindex: true
---

<meta http-equiv="refresh" content="0; URL=../analytics/product-intelligence" />

This page is the landing page for the Product Intelligence group which redirects you to the 
[new location](../analytics/product-intelligence/) for the page.
