---
layout: handbook-page-toc
title: "Developer Evangelism Team Calendar"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## <i class="fa fa-calendar" aria-hidden="true"></i> Team Calendar

The Developer Evangelism Team calendar contains team member speaking engagements, important conferences, CFP timelines, and other important dates.

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_eta7o4tn4btn8h0f8eid5q98ro%40group.calendar.google.com&ctz=Europe%2FAmsterdam" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

Developer Evangelists should add new speaking engagements to this calendar as they are scheduled. The Developer Evangelist Program Manager will manage team-wide events such as industry conferences and their CFP timelines. If you need write access to this calendar, please contact a member of the Community Relations team via Slack.